"""A fully-functional terminal emulator in a Jupyter notebook."""
__version__ = '0.2.0'
from .xterm import Xterm
from IPython.core.magic import line_magic, cell_magic, line_cell_magic, Magics, magics_class
from IPython.core import magic_arguments
import sys

def load_ipython_extension(ipython):

    obj_python=Xterm()
    obj_python.python()
    ipython.register_magics(Xterm)


