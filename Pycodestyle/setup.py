from setuptools import setup

setup(name='pycodestyle_magic',
      version='0.0.1',
      description='',
      author='Adam Johnson',
      author_email='adam.johnson@us.ibm.com',
      license='MIT',
      packages=['pycodestyle_magic'],
      install_requires=[
          'future',
      ],
      include_package_data=True,
      zip_safe=False)
